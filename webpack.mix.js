
let mix = require('laravel-mix');

mix.setPublicPath('public')
    .js('./resources/assets/js/app.js', 'public/js')
    .sass('./resources/assets/sass/app.scss', 'public/css')


mix.webpackConfig({
    resolve: {
        alias: {
            "@": path.resolve(
                __dirname,
                "resources/assets/js"
            ),
            "@scss": path.resolve(
                __dirname,
                "resources/assets/sass"
            ),
        }
    }
 });