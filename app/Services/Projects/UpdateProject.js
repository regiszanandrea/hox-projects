'use strict'

const Project = use('App/Models/Project')
const Helpers = use('Helpers')
const Drive = use('Drive')

class UpdateProject {

  static async update(project, fields, images) {
    try {
      const files = images.files
      var imagesNames = []
      for (var i = 0; i < files.length; i++) {
        imagesNames.push(files[i].clientName)
      }

      fields.images = imagesNames

      project.merge(fields)

      await project.save()

      await Drive.delete('uploads/' + project._id)

      await images.moveAll(Helpers.publicPath('uploads/' + project._id))
      if (!images.movedAll()) {
        return images.errors()
      }

      return project

    } catch (exception) {
      return exception.message
    }
  }
}


module.exports = UpdateProject
