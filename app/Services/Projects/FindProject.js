'use strict'

const Project = use('App/Models/Project')

class FindProject {

  static async find(page) {
    try {

      return await Project.query().paginate(page)

    } catch (exception) {
      return exception.message
    }
  }

  static async findById(id) {
    try {

      return await Project.find(id)

    } catch (exception) {
      return exception.message
    }
  }
}


module.exports = FindProject
