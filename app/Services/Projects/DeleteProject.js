'use strict'

const Project = use('App/Models/Project')

class DeleteProject {

  static async delete(project) {
    try {

      await project.delete()

      return project
    } catch (exception) {
      return exception.message
    }
  }

}


module.exports = DeleteProject
