'use strict'

const Project = use('App/Models/Project')
const Helpers = use('Helpers')

class CreateProject {

  static async create(fields, images, user) {
    try {
      var project = new Project()

      const files = images.files
      var imagesNames = []
      for (var i = 0; i < files.length; i++) {
        imagesNames.push(files[i].clientName)
      }

      fields.images = imagesNames

      project.fill(fields)

      await project.user().associate(user)

      await images.moveAll(Helpers.publicPath('uploads/' + project._id))
      if (!images.movedAll()) {
        return images.errors()
      }

      return project

    } catch (exception) {
      return exception.message
    }
  }

}


module.exports = CreateProject
