'use strict'

const Project = use('App/Models/Project')


class DashboardController {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({
    request,
    response,
    view
  }) {
    const projectCount = await Project.count()
    return view.render('dashboard', {
      projectCount: projectCount
    })
  }
}

module.exports = DashboardController
