'use strict'


const ProjectResponseBuilder = use('App/Builders/ProjectResponseBuilder')
const CreateProject = use('App/Services/Projects/CreateProject')
const UpdateProject = use('App/Services/Projects/UpdateProject')
const DeleteProject = use('App/Services/Projects/DeleteProject')
const FindProject = use('App/Services/Projects/FindProject')
const User = use('App/Models/User')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Project = use('App/Models/Project')

/**
 * Resourceful controller for interacting with projects
 */
class ProjectController {
  /**
   * Show a list of all projects.
   * GET projects
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({
    request,
    response,
    view
  }) {
    const bestFormat = request.accepts(['json', 'html'])

    if (bestFormat === 'json') {
      const page = request.input('page') || 1
      return await FindProject.find(page)
    }

    return view.render('projects.index')
  }

  /**
   * Render a form to be used for creating a new project.
   * GET projects/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({
    request,
    response,
    view
  }) {
    return view.render('projects.create')
  }

  /**
   * Create/save a new project.
   * POST projects
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,
    response,
    auth
  }) {
    const images = request.file('images', {
      types: ['image']
    })

    const project = await CreateProject.create(request.only(['title', 'description', 'images', 'video']), images, auth.user)

    return ProjectResponseBuilder.build(project)
  }

  /**
   * Display a single project.
   * GET projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({
    params,
    request,
    response,
    view
  }) {
    const project = await FindProject.findById(params.id)

    if (!(project instanceof Project)) {
      return response.status(404).route('projects.index')
    }

    return view.render('projects.show', {
      project: project.toJSON()
    })
  }

  /**
   * Render a form to update an existing project.
   * GET projects/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({
    params,
    request,
    response,
    view
  }) {
    const project = await FindProject.findById(params.id)

    if (!(project instanceof Project)) {
      return response.status(404).send(project)
    }

    return view.render('projects.edit', {
      project: project.toJSON()
    })
  }

  /**
   * Update project details.
   * PUT or PATCH projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({
    params,
    request,
    response
  }) {
    var project = await FindProject.findById(params.id)

    if (!(project instanceof Project)) {
      return response.status(404).route('projects.index')
    }

    const images = request.file('images', {
      types: ['image']
    })

    project = await UpdateProject.update(project, request.only(['title', 'description', 'images', 'video']), images)

    return ProjectResponseBuilder.build(project)
  }

  /**
   * Delete a project with id.
   * DELETE projects/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) {
    var project = await FindProject.findById(params.id)

    if (!(project instanceof Project)) {
      return response.route('projects.index')
    }

    project = await DeleteProject.delete(project)

    return ProjectResponseBuilder.build(project)
  }
}

module.exports = ProjectController
