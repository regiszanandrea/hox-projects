'use strict'

const User = use('App/Models/User')

class LoginController {

  async redirect({
    ally
  }) {
    await ally.driver('facebook').redirect()
  }

  async index({
    request,
    response,
    view
  }) {
    return view.render('login')
  }

  async callback({
    ally,
    auth,
    response
  }) {
    try {
      const fbUser = await ally.driver('facebook').getUser()

      // user details to be saved
      const userDetails = {
        email: fbUser.getEmail(),
        token: fbUser.getAccessToken(),
        login_source: 'facebook'
      }

      // search for existing user
      const whereClause = {
        email: fbUser.getEmail()
      }

      const user = await User.findOrCreate(whereClause, userDetails)
      await auth.login(user)

      return response.route('projects.index')
    } catch (error) {
      return response.redirect('/login')
    }
  }
}

module.exports = LoginController
