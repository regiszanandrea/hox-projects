'use strict'

const Project = use('App/Models/Project')

class CreateProject {

  static build(project) {
    if (!(project instanceof Project)) {
      return response.status(401).send(project)
    }

    return project
  }
}


module.exports = CreateProject
