'use strict'


const Model = use('Model')

class Project extends Model {

  static get hidden () {
    return ['id']
  }

	user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Project
