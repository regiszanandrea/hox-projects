'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TokensSchema extends Schema {
  up () {
    this.create('tokens', (collection) => {
      collection.index('user_id_index', {title: 1})
    })
  }

  down () {
    this.drop('tokens')
  }
}

module.exports = TokensSchema
