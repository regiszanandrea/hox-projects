'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectSchema extends Schema {
  up () {
    this.create('projects', (collection) => {
      collection.index('title_index', {title: 1})
    })
  }

  down () {
    this.drop('projects')
  }
}

module.exports = ProjectSchema
