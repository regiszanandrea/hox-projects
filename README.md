# Hox Projects

## Setup
> Requires 
>> Node.js >= 8.0.0

>> npm >= 3.0.0

1 - Install dependencies
```bash
npm install
```

2 - Copy and configure your .env
```bash
cp .env.example. env
```

3 - Run the following command to run startup migrations.

```js
adonis migration:run
```

4 - Run the server 
```js
adonis serve
```



