import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueNoty from 'vuejs-noty'

Vue.use(VueNoty,{
  theme: 'nest'
})
Vue.use(BootstrapVue)


Vue.config.productionTip = false
Vue.component('projects-index', require('./components/projects/index.vue').default)
Vue.component('projects-create', require('./components/projects/create.vue').default)
Vue.component('projects-show', require('./components/projects/show.vue').default)
Vue.component('projects-edit', require('./components/projects/edit.vue').default)

new Vue({
  el: '#app'
})
